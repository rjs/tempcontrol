// generic class for things

#ifndef ABSTRACT_REGULATOR_H
#define ABSTRACT_REGULATOR_H
class abstract_regulator {
	public:
		virtual int comm_isopened(void) = 0;
		virtual double ramp_and_wait(double final_sp, double ramp_rate, double wait_time);
		virtual double set_sp(double temperature) = 0;
		virtual double get_sp(void) = 0;
		virtual double get_pv(void) = 0;
		virtual double stop(void) = 0;
		virtual ~abstract_regulator() = 0;
};

#endif
