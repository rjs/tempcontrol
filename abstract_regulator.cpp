#include <time.h>
#include <stdio.h>
#include <math.h>

#ifdef WIN32
	#include <windows.h>
	#define delay_s(s) Sleep(1000*s)
	#define delay_ms(ms) Sleep(ms)
	#define DEGSYMBOL "\xF8"
#else
	#include <unistd.h>
	#define delay_s(s)   sleep(s)
	#define delay_ms(ms) usleep(1000*ms)
	#define DEGSYMBOL "°"
#endif

#include "abstract_regulator.h"

//#define ISNAN(x) (x != x)

// the default for devices with no "ramp" function, set_sp() in a loop!

abstract_regulator::~abstract_regulator() {}

double abstract_regulator::ramp_and_wait(double final_sp, double ramp_rate, double wait_time) {
	double initial_pv = nan(""), pv = nan(""), sp = nan(""), initial_sp = nan(""), newsp = nan("");
	time_t initial_time;
	double elapsed_time;
	double direction = nan("");
	int done;

	initial_pv = get_pv();
	initial_sp = get_sp();

	if (isnan(initial_pv) || isnan(initial_sp)) {
		initial_pv = get_pv();
		initial_sp = get_sp();
          if (isnan(initial_pv) || isnan(initial_sp)) {
			fprintf(stderr, "ERROR: Could not read controller SP or PV temperature!\a\n");
			return nan("");
		}
	}

	initial_time = time(NULL);
	if (initial_time == ((time_t)-1)) {
		perror("Failure to obtain the current time");
		return nan("");
	}

	fprintf(stderr, "Setting temperature from %.2f to %.2f", initial_sp, final_sp);
	if (ramp_rate>0) { fprintf(stderr, " (%.2f C/min)", ramp_rate); }
	if (wait_time>0) { fprintf(stderr, " with %.1f minute(s) of settling time", wait_time); }
	fprintf(stderr, ".\n");
	if (final_sp>=initial_pv) { direction = 1; }
	else { direction = -1; }

	if (ramp_rate>0) {
		done = 0;
		do {
			elapsed_time = difftime(time(NULL), initial_time) / 60;
			sp = initial_pv + (elapsed_time * ramp_rate * direction);
			if      (direction>0 && sp >= final_sp) { sp = final_sp; done=1; }
			else if (direction<0 && sp <= final_sp) { sp = final_sp; done=1; }
			newsp = set_sp(sp);
			if (isnan(newsp)) {
				fprintf(stderr, "ERROR: Could not set the temperature to %.2f...\a\n", sp);
				return newsp;
			}
			fprintf(stderr, "\rRamping %s from %.2f%sC since %.2f minutes: SP=%.2f%sC...", (direction==-1?"down":"up"), initial_pv, DEGSYMBOL, elapsed_time, newsp, DEGSYMBOL);
			delay_ms(100);
		} while (!done);
		fprintf(stderr, " done.\n");
	}
	else {
		newsp = set_sp(final_sp);
		if (isnan(newsp)) {
			fprintf(stderr, "ERROR: Could not set the temperature to %.2f...\a\n", sp);
			return newsp;
		}
	}

	if (wait_time<0) { return newsp; }

	initial_time = time(NULL);
	if (initial_time == ((time_t)-1)) {
		perror("Failure to obtain the current time");
		return nan("");
	}

	done = 0;
	do {
		pv = get_pv();
		sp = get_sp();
		if (isnan(pv) || isnan(sp)) {
			fprintf(stderr, "ERROR: Could not read controller SP or PV temperature!\a\n");
			return nan("");
		}
		if      (direction>=0 && pv >= sp) { done=1; }
		else if (direction<=0 && pv <= sp) { done=1; }
		elapsed_time = difftime(time(NULL), initial_time) / 60;
		fprintf(stderr, "\rWaiting PV=%.2f to reach SP=%.2f since %.2f minutes...", pv, sp, elapsed_time);
		delay_ms(500);
	} while (!done);
	fprintf(stderr, " ok.\n");

	initial_time = time(NULL);
	if (initial_time == ((time_t)-1)) {
		perror("Failure to obtain the current time");
		return nan("");
	}

	do {
		elapsed_time = difftime(time(NULL), initial_time) / 60;
		fprintf(stderr, "\rWaiting now %.0f minute(s). %.2f minute(s) remaining...", wait_time, wait_time-elapsed_time);
		delay_ms(500);
	} while ((wait_time-elapsed_time)>0);
	fprintf(stderr, " s!\n");

	return newsp;
}
