/*
  Name:        rkc_cb100.h
  Copyright:   UCCS CNRS 2016
  Author:      RJS
  Date:        27/09/16
  Description: module for controlling RKC CB100 temperature controllers
*/

#include "modbus.h"
#include "abstract_regulator.h"

class rkc_cb100 : public abstract_regulator {
	public:
		rkc_cb100(modbus*, unsigned char addr);
		int comm_isopened(void);
		double set_sp(double temperature);
		double get_sp(void);
		double get_pv(void);
		double stop(void);
		~rkc_cb100(void);

	private:
		modbus *commbus;
		unsigned char addr;
		double uint2dbl(unsigned int value);
		double uint2dbl(unsigned char value_high, unsigned char value_low);
};
