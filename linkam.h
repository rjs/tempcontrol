/*
  Name:        linkam.h
  Copyright:   UCCS CNRS 2016
  Author:      RJS
  Date:        17/10/16
  Description: module for controlling a Linkam T9x temperature controller
*/

#include "serial.h"
#include "abstract_regulator.h"

class linkam : public abstract_regulator {
	public:
		linkam(serial*);
		int comm_isopened(void);
		double ramp_and_wait(double final_sp, double ramp_rate, double wait_time);
		double set_sp(double temperature);
		double get_sp(void);
		double get_pv(void);
		double stop(void);
		~linkam(void);

	private:
		serial *commbus;
		double go_ramp(double final_sp, double ramp_rate);
		char linkam_status_byte, linkam_error_byte, linkam_pump_byte, linkam_gen_byte;
		double hex2dbl(const unsigned char *hexstr);
		signed int dbl_cmd(const char *command, double dbl);
};
