/*
  Name:        modbus.h
  Copyright:   UCCS CNRS 2016
  Author:      RJS
  Date:        09/05/16
  Description: modbus class, using opensource win/unix serial class
*/

#ifndef MODBUS_H
#define MODBUS_H

#include "serial.h"

class modbus : public serial {
	public:
		modbus(void);
		modbus(const char*, unsigned int);
		void enable_debug(void);
		void disable_debug(void);
		int request(const unsigned char*, size_t, unsigned char*, size_t);
		int request_one_time(const unsigned char*, size_t, unsigned char*, size_t);

	private:
	    int debug;
};

#endif
