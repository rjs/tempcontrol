/*
  Name:        rkc_cb100.c
  Author:      RJS
  Date:        09/05/16
  Description: module for controlling RKC CB100 temperature controllers
*/

#include <stdio.h>
#include <math.h>

#include "rkc_cb100.h"

rkc_cb100::rkc_cb100(modbus* modbus_connection, unsigned char addr_regulator) {
	commbus = modbus_connection;
	addr = addr_regulator;
}

int rkc_cb100::comm_isopened(void) {
	return commbus->isopened();
}

double rkc_cb100::set_sp(double temperature) {
	unsigned char req[6];
	unsigned char rxbuffer[100];
	int temp_rx;

	if (temperature<0) { temperature = 0; }
	req[0] = addr; req[1] = 6; req[2] = 0; req[3] = 6;
	req[4] = (unsigned char)(temperature/256);
	req[5] = (unsigned char)((int)temperature%256);

	if (commbus->request(req, 6, rxbuffer, sizeof rxbuffer)) { return nan(""); }
	temp_rx = 256*rxbuffer[4] + rxbuffer[5];
	if (temp_rx != (int)temperature) { return nan(""); }
	return (double)temp_rx;
}

double rkc_cb100::stop(void) {
	return set_sp(0.0);
}

double rkc_cb100::get_sp(void) {
	unsigned char req[6];
	unsigned char rxbuffer[100];

	req[0] = addr;  req[1] = 3;  req[2] = 0;
	req[3] = 6;     req[4] = 0;  req[5] = 3;

	if (commbus->request(req, 6, rxbuffer, sizeof rxbuffer)) { return nan(""); }
	if (rxbuffer[1] != 3) { return nan(""); }
	return uint2dbl(rxbuffer[3], rxbuffer[4]);
}

double rkc_cb100::get_pv(void) {
	unsigned char req[6];
	unsigned char rxbuffer[100];
	//unsigned int temp;
	double pv;

	req[0] = addr;  req[1] = 3;  req[2] = 0;
	req[3] = 0;     req[4] = 0;  req[5] = 3;

	if (commbus->request(req, 6, rxbuffer, sizeof rxbuffer)) { return nan(""); }
	if (rxbuffer[1] != 3) { return nan(""); }
	pv = uint2dbl(rxbuffer[3], rxbuffer[4]);
	if (pv>=1402) { return nan(""); } // unplug a K sensor and it returns 1402°C.
	return pv;
}

rkc_cb100::~rkc_cb100() {
	//fprintf(stderr, "rkc_cb100 ADDR=%d object destructed!\n", addr);
}

double rkc_cb100::uint2dbl(unsigned int value) {
	if (value>=32768) {
		// take a complement to 2 value to make a double
		return -(double)(32768-(value%32768));
	}
	return (double)value;
}
double rkc_cb100::uint2dbl(unsigned char value_high, unsigned char value_low) {
	unsigned int value = 256*(unsigned int)value_high + (unsigned int)value_low;
	return uint2dbl(value);
}
